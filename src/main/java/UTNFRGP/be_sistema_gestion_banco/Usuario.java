package UTNFRGP.be_sistema_gestion_banco;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public Usuario()
	{
		
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@Column(name="codigo_usuario")
	private String CodigoUsuario;
	
	@Column(name="contrasenia")
	private String Contrasenia;
	
	public String getCodigoUsuario() {
		return CodigoUsuario;
	}
	public void setCodigoUsuario(String codigoUsuario) {
		CodigoUsuario = codigoUsuario;
	}
	public String getContrasenia() {
		return Contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		Contrasenia = contrasenia;
	}

}
