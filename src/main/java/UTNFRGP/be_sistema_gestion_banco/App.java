package UTNFRGP.be_sistema_gestion_banco;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class App 
{
    public static void main( String[] args )
    {
        SessionFactory sessionFactory;
        
        Configuration configuration = new Configuration();
        configuration.configure();
        
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session session = sessionFactory.openSession();
        
        session.beginTransaction();        


        //Inserto tipos de movimientos
        TipoMovimiento tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("alta_cuenta");
        tipoMovimiento.setDescripcion("Alta de cuenta");
        session.save(tipoMovimiento);

        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("alta_prestamo");
        tipoMovimiento.setDescripcion("Alta de prestamo");
        session.save(tipoMovimiento);
        
        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("pago_prestamo");
        tipoMovimiento.setDescripcion("Pago de prestamo");
        session.save(tipoMovimiento);
        
        tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setCodigo("transferencia");
        tipoMovimiento.setDescripcion("Transferencia");
        session.save(tipoMovimiento);
        
        
        
        //Inserto tipos de cuentas
        TipoCuenta tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("ca_pesos");
        tipoCuenta.setDescripcion("Caja de ahorro en pesos");
        session.save(tipoCuenta);

        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("ca_dolares");
        tipoCuenta.setDescripcion("Caja de ahorro en dolares");
        session.save(tipoCuenta);
        
        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc");
        tipoCuenta.setDescripcion("Cuenta corriente");
        session.save(tipoCuenta);
        
        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc_esp_pesos");
        tipoCuenta.setDescripcion("Cuenta corriente especial en pesos");
        session.save(tipoCuenta);

        tipoCuenta = new TipoCuenta();
        tipoCuenta.setCodigo("cc_esp_dolares");
        tipoCuenta.setDescripcion("Cuenta corriente especial en dolares");
        session.save(tipoCuenta);

        
        
        
        //Inserto usuarios, clientes y administradores
        Usuario usuario = new Usuario();
        Cliente cliente = new Cliente();
        Administrador administrador = new Administrador();
        Cuenta cuenta = new Cuenta();
        usuario.setCodigoUsuario("jdelatorre");
        usuario.setContrasenia("jdelatorre");
        administrador.setCodigo("jdelatorre");
        administrador.setApellido("de la Torre");
        administrador.setNombre("Joaquin Lino");
        administrador.setDni("33333333");
        administrador.setDireccion("Calle X 2222 - Caba");
        administrador.setFechaNacimiento(19889424);
        administrador.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("fconde");
        usuario.setContrasenia("fconde");
        cliente.setCodigo("fconde");
        cliente.setApellido("Conde");
        cliente.setNombre("Fernanda");
        cliente.setDni("11111111");
        cliente.setDireccion("Calle 1 1111");
        cliente.setFechaNacimiento(19790101);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("eamadeo");
        usuario.setContrasenia("eamadeo");
        cliente.setCodigo("eamadeo");
        cliente.setApellido("Amadeo");
        cliente.setNombre("Estela");
        cliente.setDni("22222222");
        cliente.setDireccion("Calle 2 2222");
        cliente.setFechaNacimiento(19920202);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("mgarcia");
        usuario.setContrasenia("mgarcia");
        cliente.setCodigo("mgarcia");
        cliente.setApellido("Garcia");
        cliente.setNombre("Maria");
        cliente.setDni("33333333");
        cliente.setDireccion("Calle 3 3333");
        cliente.setFechaNacimiento(19930303);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("osiames");
        usuario.setContrasenia("osiames");
        cliente.setCodigo("osiames");
        cliente.setApellido("Siames");
        cliente.setNombre("Ozzito");
        cliente.setDni("44444444");
        cliente.setDireccion("Calle 4 4444");
        cliente.setFechaNacimiento(20130101);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("uovejera");
        usuario.setContrasenia("uovejera");
        cliente.setCodigo("uovejera");
        cliente.setApellido("Ovejera");
        cliente.setNombre("Uma");
        cliente.setDni("55555555");
        cliente.setDireccion("Calle 5 5555");
        cliente.setFechaNacimiento(20121505);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);

        usuario = new Usuario();
        cliente = new Cliente();
        usuario.setCodigoUsuario("cdelatorre");
        usuario.setContrasenia("cdelatorre");
        cliente.setCodigo("cdelatorre");
        cliente.setApellido("de la Torre");
        cliente.setNombre("Carlos Lino");
        cliente.setDni("66666666");
        cliente.setDireccion("Calle 6 6666");
        cliente.setFechaNacimiento(19160101);
        cliente.setUsuario(usuario);
        session.save(usuario);
        session.save(cliente);
        

        
        //Inserto cuentas y los movimientos de alta
        String hql = "from TipoCuenta where codigo='ca_pesos'";
        TipoCuenta ca_pesos = (TipoCuenta) session.createQuery(hql).uniqueResult();
        hql = "from Cliente where codigo='jdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        hql = "from TipoMovimiento where codigo='alta_cuenta'";
        TipoMovimiento alta_cuenta = (TipoMovimiento) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("11111111");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("11111111");
        cuenta.setDescripcion("");
        cuenta.setCbu("1111111111111111111111");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 11111111");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);

        Movimiento movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(11111.33);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);


        hql = "from Cliente where codigo='uovejera'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("22222222");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("22222222");
        cuenta.setDescripcion("");
        cuenta.setCbu("2222222222222222222222");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 22222222");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(22332.654);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);


        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("33333333");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("33333333");
        cuenta.setDescripcion("");
        cuenta.setCbu("3333333333333333333333");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 33333333");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(64573563);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        
        
        hql = "from Cliente where codigo='cdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("44444444");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("44444444");
        cuenta.setDescripcion("");
        cuenta.setCbu("4444444444444444444444");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 44444444");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(12312.123);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        
        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        cuenta = new Cuenta();
        cuenta.setTipoCuenta(ca_pesos);
        cuenta.setNroCuenta("55555555");
        cuenta.setCliente(cliente);
        cuenta.setCodigo("55555555");
        cuenta.setDescripcion("");
        cuenta.setCbu("5555555555555555555555");
        cuenta.setSaldo(100000.55);
        cuenta.setNombre("Cuenta 55555555");
        cuenta.setFechaCreacion(20200607);
        session.save(cuenta);
        
        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_cuenta);
        movimiento.setDetalle("alta de cuenta");
        movimiento.setFecha(20200607);
        movimiento.setImporte(12345.123);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);

        

        //Inserto prestamos y cuotas
        Prestamo prestamo = new Prestamo();
        Cuota cuota;
        hql = "from Cliente where codigo='cdelatorre'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        hql = "from TipoMovimiento where codigo='alta_prestamo'";
        TipoMovimiento alta_prestamo = (TipoMovimiento) session.createQuery(hql).uniqueResult();

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(345345345);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        prestamo.setCliente(cliente);
        prestamo.setMovimiento(movimiento);
        prestamo.setCuotas(3);
        prestamo.setFecha(20200605);
        prestamo.setImporte(3000);
        prestamo.setMontoPorMes(prestamo.getImporte() / prestamo.getCuotas());
        prestamo.setPlazo(3);

        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        prestamo = new Prestamo();
        hql = "from Cliente where codigo='uovejera'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(543634);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        prestamo.setCliente(cliente);
        prestamo.setMovimiento(movimiento);
        prestamo.setCuotas(4);
        prestamo.setFecha(20200605);
        prestamo.setImporte(8000);
        prestamo.setMontoPorMes(prestamo.getImporte() / prestamo.getCuotas());
        prestamo.setPlazo(4);
        
        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        prestamo = new Prestamo();
        hql = "from Cliente where codigo='eamadeo'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();

        movimiento = new Movimiento();
        movimiento.setTipoMovimiento(alta_prestamo);
        movimiento.setDetalle("alta de prestamo");
        movimiento.setFecha(20200607);
        movimiento.setImporte(543634);
        movimiento.setCuenta(cuenta);
        session.save(movimiento);
        
        prestamo.setMovimiento(movimiento);
        prestamo.setCliente(cliente);
        prestamo.setCuotas(2);
        prestamo.setFecha(20200605);
        prestamo.setImporte(2000);
        prestamo.setMontoPorMes(prestamo.getImporte() / prestamo.getCuotas());
        prestamo.setPlazo(2);

        for(int i = 1; i <= prestamo.getCuotas(); i++) {
            cuota = new Cuota();
            cuota.setPrestamo(prestamo);
            cuota.setNroCuota(i);
            session.save(cuota);        	
        }

        
        hql = "from TipoMovimiento where codigo='transferencia'";
        TipoMovimiento tipo_transferencia = (TipoMovimiento) session.createQuery(hql).uniqueResult();
        hql = "from Cliente where codigo='osiames'";
        cliente = (Cliente) session.createQuery(hql).uniqueResult();
        
        hql = "from Cuenta where Cliente = '" + cliente.getId() + "'";
        Cuenta origen = (Cuenta) session.createQuery(hql).uniqueResult();
        
        hql = "from Cuenta where Cbu='5555555555555555555555'";
        Cuenta destino = (Cuenta) session.createQuery(hql).uniqueResult();
        
        Transferencia transferencia = new Transferencia();
        double importe = 543634;
        transferencia.setCliente(cliente);
        transferencia.setImporte(importe);
        
        Movimiento movimientoNeg = new Movimiento();
        movimientoNeg.setTipoMovimiento(tipo_transferencia);
        movimientoNeg.setDetalle("transferencia - saldo negativo");
        movimientoNeg.setFecha(20200607);
        movimientoNeg.setImporte(-importe);
        movimientoNeg.setCuenta(origen);
        session.save(movimientoNeg);

        Movimiento movimientoPos = new Movimiento();
        movimientoPos.setTipoMovimiento(tipo_transferencia);
        movimientoPos.setDetalle("transferencia - saldo positivo");
        movimientoPos.setFecha(20200607);
        movimientoPos.setImporte(importe);
        movimientoPos.setCuenta(destino);
        session.save(movimientoPos);

        transferencia.setSaldoNegativo(movimientoNeg);
        transferencia.setSaldoPositivo(movimientoPos);
        transferencia.setFecha(20200608);
        session.save(transferencia);
        
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
